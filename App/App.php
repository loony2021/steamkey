<?php

namespace App;


class App
{
    public static $helper;
    public static $db;

    public static function bootstrap(): void
    {
        require __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Config' . DIRECTORY_SEPARATOR . 'global.php';


        $router = new \Core\Router();
        static::$db = new \Core\Database();
        self::$helper = new Helper();

        $router->launch();
    }
}
