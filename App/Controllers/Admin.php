<?php


namespace Controllers;


use App\Helper;

use App\DAL\OrderDAO;
use App\DAL\UserDAO;
use App\DAL\GameDAO;

class Admin
{
    private $orderDAO;
    private $userDAO;
    private $gameDAO;

    public function __construct()
    {
        $this->orderDAO = new \DAL\OrderDAO();
        $this->userDAO = new \DAL\UserDAO();
        $this->gameDAO = new \DAL\GameDAO();
    }

    public function login()
    {
        if (Helper::isLoggin()) {
            Helper::redirect('/adminPanel');
        }
        require __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'View' . DIRECTORY_SEPARATOR . 'admin.php';
    }

    public function adminPanel()
    {
        Helper::requireLogIn();
        $orders = $this->orderDAO->getAll();
        $customers = $this->userDAO->getAll();
        $games = $this->gameDAO->getAll();
        require __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'View' . DIRECTORY_SEPARATOR . 'adminPanel.php';

    }
}