<?php


namespace Controllers;


//use App\DAL\GameDAO;

class Home
{
    private $gameDAO;

    public function __construct()
    {
        $this->gameDAO = new \DAL\GameDAO();

    }

    public function home()
    {

        $games = $this->gameDAO->getAll();
        function backgroundBlack() {
            if (preg_match('/fbclid/', $_SERVER['REQUEST_URI'])) {
                echo 'style="background: black;"';
            }

        }
        require __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'View' . DIRECTORY_SEPARATOR . 'index.php';
    }
}