<?php

namespace Core;

use Config\DatabaseConfig;
use PDO;
use PDOException;

class Database
{
    private $host = DatabaseConfig::DB_HOST;
    private $user = DatabaseConfig::DB_USER;
    private $pass = DatabaseConfig::DB_PASS;
    private $dbname = DatabaseConfig::DB_NAME;

    public $dbh;
    private $stmt;
    private $error;

    public function __construct()
    {
        $dsn = "mysql:host=$this->host;dbname=$this->dbname";
        $option = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ];
        try {
            $this->dbh = new PDO($dsn, $this->user, $this->pass, $option);
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            echo $this->error;
        }
    }
}