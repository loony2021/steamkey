<?php

namespace Core;



use Config\Routs;

class Router
{
    public $errorPath = [
        'Controllers\Error',
        'error404'
    ];

    public function launch()
    {

        if (($pos = strpos($_SERVER['REQUEST_URI'], '?')) !== false) {
            $uri = substr($_SERVER['REQUEST_URI'], 0, $pos);
        }

        $uri = $uri ?? $_SERVER['REQUEST_URI'];

        $route = Routs::APP_ROUTS[$uri];
        $uri = $this->routPars($route);

        if ($uri[0] === '') {
            $controllerName = $this->errorPath[0];
            $actionName = $this->errorPath[1];
        } else {
            $controllerName = $uri[0];
            $actionName = $uri[1];
        }

        $controller = new $controllerName;

        return $controller->$actionName();
    }

    public function routPars($route)
    {
        $route = explode('@', $route);
        $path[0] = array_shift($route);
        $path[1] = array_shift($route);
        return $path;
    }

}