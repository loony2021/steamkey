<?php


namespace DAL;


use App\App;
use PDO;

class AdminDAO
{
    private $db;

    public function __construct()
    {
        $this->db = App::$db;
    }

    public function login($data) {
        $query = 'SELECT * FROM `AdminSteamKey` WHERE `login` LIKE :login AND `password` LIKE :password';
        $sth = $this->db->dbh->prepare($query);
        $sth->bindParam(':login', $data['login']);
        $sth->bindParam(':password', $data['password']);
        $sth->execute();
        return $sth->fetch(PDO::FETCH_ASSOC);
    }
}