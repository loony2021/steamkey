<?php


namespace DAL;


use App\App;
use PDO;

class GameDAO
{
    private $db;

    public function __construct()
    {
        $this->db = App::$db;
    }

    public function create($data)
    {
        $query = 'INSERT INTO `Games` (`game`, `count`, `category`, `description`) VALUES (:game, :count, :category, :description);';
        $sth = $this->db->dbh->prepare($query);
        $sth->bindParam(':game', $data['game']);
        $sth->bindParam(':count', $data['count']);
        $sth->bindParam(':category', $data['category']);
        $sth->bindParam(':description', $data['description']);
        return $sth->execute();
    }

    public function getAll()
    {
        $query = 'SELECT * FROM `Games` ORDER BY `Games`.`popularity` DESC';
        $sth = $this->db->dbh->prepare($query);
        $sth->execute();
        return $sth->fetchAll();
    }

    public function find($game)
    {
        $query = 'SELECT * FROM `Games` WHERE `game` LIKE :game';
        $sth = $this->db->dbh->prepare($query);
        $sth->bindParam(':game', $game);
        $sth->execute();
        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    public function orderGame($game)
    {
        $count = $game['count'] - 1;
        $popularity = $game['popularity'] + 1;
        $query = 'UPDATE `Games` SET `count` = :count, `popularity` = :popularity WHERE `Games`.`id` = :id';
        $sth = $this->db->dbh->prepare($query);
        $sth->bindParam(':popularity', $popularity);
        $sth->bindParam(':count', $count);
        $sth->bindParam(':id', $game['id']);
        return $sth->execute();
    }
}