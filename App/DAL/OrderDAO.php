<?php


namespace DAL;

use App\App;

class OrderDAO
{

    private $db;

    public function __construct()
    {
        $this->db = App::$db;
    }

    public function create($data)
    {
        $query = 'INSERT INTO `orders` (`name`, `email`, `games`, `ip`) VALUES (:name, :email, :games, :ip);';
        $sth = $this->db->dbh->prepare($query);
        $sth->bindParam(':name', $data['name']);
        $sth->bindParam(':email', $data['email']);
        $sth->bindParam(':games', $data['games']);
        $sth->bindParam(':ip', $data['ip']);
        return $sth->execute();
    }

    public function getAll()
    {
        $query = 'SELECT * FROM `orders`';
        $sth = $this->db->dbh->prepare($query);
        $sth->execute();
        return $sth->fetchAll();
    }
}