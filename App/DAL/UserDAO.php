<?php


namespace DAL;


use App\App;
use PDO;

class UserDAO
{

    private $db;

    public function __construct()
    {
        $this->db = App::$db;
    }
    public function find($data) {
        $query = 'SELECT * FROM `Customers` WHERE `email` LIKE :email ';
        $sth = $this->db->dbh->prepare($query);
        $sth->bindParam(':email', $data['email']);
        $sth->execute();
        return $sth->fetch(PDO::FETCH_ASSOC);
    }
    public function create($data) {
        $query = 'INSERT INTO `Customers` (`name`, `email`, `ip`) VALUES (:name, :email, :ip);';
        $sth = $this->db->dbh->prepare($query);
        $sth->bindParam(':name', $data['name']);
        $sth->bindParam(':email', $data['email']);
        $sth->bindParam(':ip', $data['ip']);
        return $sth->execute();
    }
    public function getAll() {
        $query = 'SELECT * FROM `Customers`';
        $sth = $this->db->dbh->prepare($query);
        $sth->execute();
        return $sth->fetchAll();
    }
}