<?php


namespace App;


class Helper
{
    public static function is_post_request(): ?bool
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            return true;
        }
        return false;
    }
    public static function redirect($uri): void
    {
        header('Location: ' . $uri);
        exit;
    }
    public static function isLoggin() {
        if ($_COOKIE["TestCookie"])
        {
            return true;
        }
        return false;
    }
    public static function logIn() {
        setcookie("TestCookie", 'Admin', time()+3600);
        return true;
    }

    public static function logOut() {
        setcookie ("TestCookie", "", time() - 3600);
    }

    public static function requireLogIn() {
        if (self::isLoggin()) {
            return true;
        }
        self::redirect('/adminPanelLogin');
    }
}