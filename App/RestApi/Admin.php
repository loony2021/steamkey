<?php


namespace RestApi;

use App\Helper;
use App\DAL\AdminDAO;


class Admin
{
    private $adminDAO;

    public function __construct()
    {
        $this->adminDAO = new \DAL\AdminDAO();
    }

    public function login()
    {
        if (Helper::is_post_request()) {
            $data = [
                'login' => htmlentities(stripslashes(trim($_POST['login']))),
                'password' => htmlentities(stripslashes(trim($_POST['password']))),
                'loginError' => '',
                'passwordError' => ''
            ];
            if (isset($data['login']) && isset($data['password'])) {
                if ($this->adminDAO->login($data)) {
                    Helper::logIn();
                    echo true;
                }
            }
        }

    }

    public function logout()
    {
        Helper::logOut();
        Helper::redirect('/adminPanelLogin');
    }
}