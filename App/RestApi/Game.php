<?php


namespace RestApi;


use App\Helper;

class Game
{
    private $gameDAO;

    public function __construct()
    {
        $this->gameDAO = new \DAL\GameDAO();

    }

    public function create()
    {
        if (Helper::is_post_request()) {
            $data = [
                'game' => htmlentities(stripslashes(trim($_POST['game']))),
                'count' => htmlentities(stripslashes(trim($_POST['count']))),
                'category' => htmlentities(stripslashes(trim($_POST['category']))),
                'description' => htmlentities(stripslashes(trim($_POST['description']))),
                'gameError' => '',
                'countError' => '',
                'categoryError' => '',
                'descriptionError' => ''
            ];
            $this->gameDAO->create($data);
        }
    }

    public function checkCount($game) {
        $game = $this->gameDAO->find(trim($game));
        if ($game['count'] < 0)
        {
            return false;
        }
        return $game;
    }


}