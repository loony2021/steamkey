<?php


namespace RestApi;


use App\DAL\UserDAO;

class Mail
{
    private $userDAO;

    public function __construct()
    {
        $this->userDAO = new \DAL\UserDAO();
    }

    public function sendOrder($data)
    {
        $to = $data['email'];

        $subject = "Подтверждение заказа";

        $message = ' <p>Здравствуйте, ' . $data['name'] . '</p> </br> <p>Спасибо за ваш заказ, проверте список игр перед пока с вами свяжется нас менеджер.</p> </br>
                <p>Вы заказали ' . $data['games'] . '</p> </br>';

        $headers = "Content-type: text/html; charset=\"utf-8\r\n";
        $headers .= "From: SteamKey.tezgroup.org - info@steamkey.tezgroup.org\r\n";
        $headers .= "Reply-To: info@steamkey.tezgroup.org\r\n";

        mail($to, $subject, $message, $headers);
    }

    public function sendMassage()
    {
        $to = $_POST['email'];

        $subject = $_POST['sub'];

        $message = $_POST['text'];

        $headers = "Content-type: text/html; charset=\"utf-8\r\n";
        $headers .= "From: SteamKey.tezgroup.org - info@steamkey.tezgroup.org\r\n";
        $headers .= "Reply-To: info@steamkey.tezgroup.org\r\n";

        mail($to, $subject, $message, $headers);

        echo 'Сообщение отправлено';
    }

    public function gmailSpam()
    {
        $users = $this->userDAO->getAll();
        $email = '';
        foreach ($users as $user) {
            if (preg_match("/^gmail\.com$/", $user['email'])) {
                $email .= ' ' . $user['email'];
            }
        }
    }
}