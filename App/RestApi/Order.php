<?php


namespace RestApi;

use App\DAL\OrderDAO;
use App\DAL\UserDAO;
use App\DAL\GameDAO;
use App\Helper;


class Order
{
    private $orderDAO;
    private $userDAO;
    private $gameDAO;
    private $gameApi;
    private $mail;

    public function __construct()
    {
        $this->orderDAO = new \DAL\OrderDAO();
        $this->gameDAO = new \DAL\GameDAO();
        $this->gameApi = new Game();
        $this->userDAO = new \DAL\UserDAO();
        $this->mail = new Mail();
    }

    public function create()
    {
        if (Helper::is_post_request()) {
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            $data = [
                'name' => htmlentities(stripslashes(trim($_POST['name']))),
                'email' => htmlentities(stripslashes(trim($_POST['email']))),
                'games' => htmlentities(stripslashes(trim($_POST['games']))),
                'ip' => $ip,
                'nameError' => '',
                'emailError' => '',
                'gamesError' => '',
            ];
            $ordergames = array_filter(explode(',', $data['games']));
            foreach ($ordergames as $game) {
                if (!$this->gameApi->checkCount($game)) {
                    $data['gamesError'] = $data['gamesError'] . $game . ' нет в наличии.';
                }
            }
            if (!preg_match('/@/', $data['email'])) {
                $data['emailError'] = 'Неверный Email';
            }
            if (!preg_match('/^[a-zA-Zа-яА-Я\'][a-zA-Zа-яА-Я-\' ]+[a-zA-Zа-яА-Я\']?$/u', $data['name'])) {
                $data['nameError'] = 'Неверное имя';
            }

            if (isset($data['email']) && isset($data['name']) && isset($data['games']) && empty($data['nameError']) && empty($data['emailError']) && empty($data['gamesError'])) {
                if (empty($this->userDAO->find($data))) {
                    $this->userDAO->create($data);
                }
                if ($this->orderDAO->create($data)) {
                    $this->mail->sendOrder($data);
                    $games = $this->gameDAO->getAll();
                    foreach ($ordergames as $game) {
                        $name = $this->gameDAO->find(trim($game));
                        $this->gameDAO->orderGame($name);

                    }
                    echo 'Спасибо что заказли игры у нас, подтвердите заказ на почте';
                    exit;
                }

            }
            echo $data['gamesError'];
            exit;
        }
    }
}