<div class="chartjs-size-monitor"
     style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
    <div class="chartjs-size-monitor-expand"
         style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
        <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
    </div>
    <div class="chartjs-size-monitor-shrink"
         style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
        <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
    </div>
</div>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Dashboard</h1>
</div>

<canvas class="my-4 w-100 chartjs-render-monitor" id="myChart" width="649" height="200"
        style="display: block; width: 649px; height: 200px;"></canvas>

<h2>Last orders</h2>
<div class="table-responsive">
    <table class="table table-striped table-sm">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Games</th>
            <th>Ip</th>
            <th>Created At</th>
        </tr>
        </thead>
        <tbody>
        <?php
        /** @var $orders */
        $fiveOrders = array_slice($orders, -5, 5);
        arsort($fiveOrders);
        foreach ($fiveOrders as $order) {
            echo '<tr>
            <td>' . $order['id'] . '</td>
            <td>' . $order['name'] . '</td>
            <td>' . $order['email'] . '</td>
            <td>' . $order['games'] . '</td>
            <td>' . $order['ip'] . '</td>
            <td>' . $order['created_at'] . '</td>
        </tr>';
            }
        ?>

        </tbody>
    </table>
</div>