<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Games</h1>
    <div class="btn-toolbar mb-2 mb-md-0 contact">
<!--        <form>-->
<!--            <div class="row">-->
<!--                <div class="col">-->
<!--                    <label for="game">Game name</label>-->
<!--                    <input id="game" name="name" type="text" class="form-control" placeholder="Name">-->
<!--                </div>-->
<!--                <div class="col">-->
<!--                    <label for="count">Count of game</label>-->
<!--                    <input id="count" name="count" type="text" class="form-control" placeholder="Count">-->
<!--                </div>-->
<!--                <div class="col">-->
<!--                    <label for="File">Image game</label>-->
<!--                    <input type="file" name="files" class="form-control-file" id="File">-->
<!--                </div>-->
<!--                <div class="col">-->
<!--                    <label for="category">Category</label>-->
<!--                    <input id="category" name="Category" type="text" class="form-control" placeholder="Category">-->
<!--                </div>-->
<!--                <div class="col">-->
<!--                    <label for="description">Description</label>-->
<!--                    <input id="description" name="Description" type="text" class="form-control" placeholder="Description">-->
<!--                </div>-->
<!--                <div class="col btn-group mr-2 ml-2">-->
<!--                    <button type="button" class="btn btn-sm btn-outline-secondary">Add</button>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!--        </form>-->
        <form action="/addGameRestApi" method="post" role="form" class="php-email-form">
            <div class="form-row">
                <div class="col form-group">
                    <input type="text" name="game" class="form-control" id="game" placeholder="Name"
                           data-rule="minlen:4" data-msg="Please enter a valid game">
                    <div class="validate"></div>
                </div>
                <div class="col form-group">
                    <input type="text" class="form-control" name="count" id="count"
                           placeholder="Number of game" data-rule="minlen:1" data-msg="Please enter a valid count">
                    <div class="validate"></div>
                </div>
                <div class="col form-group">
                    <input type="text" class="form-control" name="category" id="category"
                           placeholder="Game categories" data-rule="minlen:1" data-msg="Please enter a valid category">
                    <div class="validate"></div>
                </div>
                <div class="col form-group">
                    <input type="text" class="form-control" name="description" id="description"
                           placeholder="Game description" data-rule="minlen:1" data-msg="Please enter a valid description">
                    <div class="validate"></div>
                </div>
                <div class="col text-center">
                    <button type="submit">Send Message</button>
                </div>
            </div>
        </form>

    </div>
</div>
<div class="table-responsive">
    <table class="table table-striped table-sm">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Count</th>
            <th>popularity</th>
            <th>category</th>
            <th>description</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($games as $game) {
        echo '<tr>
            <td>' . $game['id'] . '</td>
            <td>' . $game['game'] . '</td>
            <td>' . $game['count'] . '</td>
            <td>' . $game['popularity'] . '</td>
            <td>' . $game['category'] . '</td>
            <td>' . $game['description'] . '</td>
        </tr>';
        }
        ?>
        </tbody>
    </table>
</div>