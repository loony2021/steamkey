<?php use function Controllers\backgroundBlack;?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>SteamKey - лучшие игры, лучшие цены</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <!--    <link href="../../public/img/favicon.png" rel="icon">-->
    <!--    <link href="../../public/img/apple-touch-icon.png" rel="apple-touch-icon">-->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
          rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="../../public/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../public/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="../../public/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="../../public/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="../../public/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="../../public/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="../../public/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="../../public/css/style.css" rel="stylesheet">
    <?php
    if(preg_match('/fbclid/', $_SERVER['REQUEST_URI'])) {
        echo '<style>
        .features .nav-link.active {
            background: red;
            color: black;
        }
    </style>';
    }
    ?>
    <!-- =======================================================
    * Template Name: Sailor - v2.0.0
    * Template URL: https://bootstrapmade.com/sailor-free-bootstrap-theme/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
</head>

<body>

<!-- ======= Header ======= -->
<header <?php backgroundBlack(); ?> id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">

        <h1 class="logo"><a href="/">Steamkey</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.php" class="logo"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

        <nav class="nav-menu d-none d-lg-block">

            <ul>
                <li class="active"><a href="index.php">Главная</a></li>

                <!--                <li class="drop-down"><a href="#">Игры по жанрам</a>-->
                <!--                    <ul>-->
                <!--                        <li><a href="#">Экшн</a></li>-->
                <!--                        <li><a href="#">Стратегии</a></li>-->
                <!--                        <li><a href="#">Платформеры</a></li>-->
                <!---->
                <!--                        <li class="drop-down"><a href="#">Deep Drop Down</a>-->
                <!--                            <ul>-->
                <!--                                <li><a href="#">Deep Drop Down 1</a></li>-->
                <!--                                <li><a href="#">Deep Drop Down 2</a></li>-->
                <!--                                <li><a href="#">Deep Drop Down 3</a></li>-->
                <!--                                <li><a href="#">Deep Drop Down 4</a></li>-->
                <!--                                <li><a href="#">Deep Drop Down 5</a></li>-->
                <!--                            </ul>-->
                <!--                        </li>-->
                <!--                    </ul>-->
                <!--                </li>-->

                <li><a href="#features">Лучшие игры 2020</a></li>
                <li><a href="#portfolio">Игры по жанрам</a></li>
                <li><a href="#">Цены</a></li>
                <li><a href="#contact">Заказать</a></li>
                <li><a href="#">Контакты</a></li>

            </ul>

        </nav><!-- .nav-menu -->

        <a href="/" class="get-started-btn ml-auto">Get Started</a>

    </div>
</header><!-- End Header -->
