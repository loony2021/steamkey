<section <?php use function Controllers\backgroundBlack; backgroundBlack(); ?> id="contact" class="contact">
    <div class="container">
        <div class="row mt-5">
            <div class="col-12 mt-5 mt-lg-0">

                <form <?php backgroundBlack(); ?> action="/orderRestApi" method="post" role="form" class="php-email-form">
                    <div <?php backgroundBlack(); ?> class="form-row">
                        <div class="col-md-6 form-group">
                            <input <?php backgroundBlack(); ?> type="text" name="name" class="form-control" id="name" placeholder="Your Name"
                                   data-rule="minlen:4" data-msg="Please enter at least 4 chars">
                            <div class="validate"></div>
                        </div>
                        <div class="col-md-6 form-group">
                            <input <?php backgroundBlack(); ?> type="email" class="form-control" name="email" id="email"
                                   placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email">
                            <div class="validate"></div>
                        </div>
                    </div>
                    <div <?php backgroundBlack(); ?> class="form-group">
                        <label for="gameList">Введите игры через запятую</label>
                            <textarea <?php backgroundBlack(); ?> id="gameList" class="form-control" name="games" rows="5" data-rule="minlen:3"
                                      data-msg="Please write something for us" placeholder="Message"></textarea>
                        <div class="validate"></div>
                    </div>
                    <div <?php backgroundBlack(); ?> class="mb-3">
                        <div class="loading">Loading</div>
                        <div class="error-message"></div>
                        <div class="sent-message">Your message has been sent. Thank you!</div>
                        <?php /** @var $games */
                        $fiveGames = array_slice($games, 0, 5);
                        foreach ($fiveGames as $game) {
                            echo '<button type="button" class="btn btn-danger mr-2 game' . $game['id'] . '">' . $game['game'] . '</button>';
                        }
                        ?>

                    </div>

                    <div class="text-center">
                        <button type="submit">Send Message</button>
                    </div>
                </form>

            </div>

        </div>

    </div>
</section>