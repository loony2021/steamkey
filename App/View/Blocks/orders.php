<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Orders</h1>
</div>
<div class="table-responsive">
    <table class="table table-striped table-sm">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Games</th>
            <th>Ip</th>
            <th>Created At</th>
        </tr>
        </thead>
        <tbody>
        <?php
        /** @var $orders */
        arsort($orders);
        foreach ($orders as $order) {
            echo '<tr>
            <td>' . $order['id'] . '</td>
            <td>' . $order['name'] . '</td>
            <td>' . $order['email'] . '</td>
            <td>' . $order['games'] . '</td>
            <td>' . $order['ip'] . '</td>
            <td>' . $order['created_at'] . '</td>
        </tr>';
        }
        ?>

        </tbody>
    </table>
</div>