<section id="hero">
    <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

            <!-- Slide 1 -->
            <div class="carousel-item active" style="background-image: url(../../public/img/rdr2.jpg)">
                <div class="carousel-container">
                    <div class="container">
                        <h2 class="animate__animated animate__fadeInDown">Red Dead Redemption 2</h2>
                        <p class="animate__animated animate__fadeInUp">эпическое приключение в открытом мире от Rockstar
                            Games, получившее признание критиков и самый высокий рейтинг среди игр</p>
                        <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Купить
                            игру</a>
                    </div>
                </div>
            </div>

            <!-- Slide 2 -->
            <div class="carousel-item" style="background-image: url(../../public/img/cp2077.jpg)">
                <div class="carousel-container">
                    <div class="container">
                        <h2 class="animate__animated animate__fadeInDown">Cyperpunk 2077</h2>
                        <p class="animate__animated animate__fadeInUp">приключенческая ролевая игра, действие которой
                            происходит в мегаполисе Найт-Сити, где власть, роскошь и модификации тела ценятся выше
                            всего. Вы играете за V, наёмника в поисках устройства, позволяющего обрести бессмертие.</p>
                        <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Купить
                            игру</a>
                    </div>
                </div>
            </div>

            <!-- Slide 3 -->
            <div class="carousel-item" style="background-image: url(../../public/img/mab2.jpg)">
                <div class="carousel-container">
                    <div class="container">
                        <h2 class="animate__animated animate__fadeInDown">Mount and Blade 2: Bannerlord</h2>
                        <p class="animate__animated animate__fadeInUp">Звучат горны, слетаются вороны. Гражданская война
                            раздирает империю. Новые королевства возникают за её пределами. Хватайте меч, надевайте
                            доспехи, собирайте своих последователей и отправляйтесь завоёвывать славу на полях сражений
                            в Кальрадии.</p>
                        <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Купить игру</a>
                    </div>
                </div>
            </div>

        </div>

        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon icofont-simple-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon icofont-simple-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>

    </div>
</section><!-- End Hero -->