<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>Floating labels example · Bootstrap</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/floating-labels/">

    <!-- Bootstrap core CSS -->
    <link href="../../public/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="../../public/css/login.css" rel="stylesheet">
</head>
<body style="">
<form class="form-signin">
    <div class="text-center mb-4">
        <h1 class="h3 mb-3 font-weight-normal">Admin panel</h1>
    </div>

    <div class="form-label-group">
        <input type="text" name="login" id="inputLogin" class="form-control" placeholder="Login" required="" autofocus="">
        <label for="inputLogin">Email address</label>
    </div>

    <div class="form-label-group">
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required="">
        <label for="inputPassword">Password</label>
    </div>
    <button id="submit" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
</form>


<script src="../public/vendor/jquery/jquery.min.js"></script>
<!--<script src="../public/js/adminLogin.js"></script>-->
<script>
    $(document).ready(function() {
        $('form').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: '/adminRestApiLogin',
                data: $(this).serialize(),
                success: function(response)
                {
                    var jsonData = JSON.parse(response);

                    // user is logged in successfully in the back-end
                    // let's redirect
                    if (jsonData.success == "1")
                    {

                    }
                    else
                    {
                        $(location).attr('href','/adminPanel/dashboard');
                    }
                }
            });
        });
    });
</script>
</body>
</html>