<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>Dashboard Template · Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="../../public/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href="../../public/css/style.css" rel="stylesheet">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="../../public/css/adminPanel.css" rel="stylesheet">
    <style type="text/css">/* Chart.js */
        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }</style>
</head>
<body style="">
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="#">SteamKey</a>
    <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse"
            data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="/adminRestApiLogout">Sign out</a>
        </li>
    </ul>
</nav>

<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
            <div class="sidebar-sticky pt-3">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link <?php use Controllers\Admin;

                        if ($_SERVER['REQUEST_URI'] == '/adminPanel/dashboard') {
                            echo 'active';
                        } ?>" href="https://steamkey.tezgroup.org/adminPanel/dashboard">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            Dashboard <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item orders">
                        <a class="nav-link <?php if ($_SERVER['REQUEST_URI'] == '/adminPanel/orders') {
                            echo 'active';
                        } ?>" href="https://steamkey.tezgroup.org/adminPanel/orders">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-file">
                                <path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path>
                                <polyline points="13 2 13 9 20 9"></polyline>
                            </svg>
                            Orders
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if ($_SERVER['REQUEST_URI'] == '/adminPanel/games') {
                            echo 'active';
                        } ?>" href="https://steamkey.tezgroup.org/adminPanel/games">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-shopping-cart">
                                <circle cx="9" cy="21" r="1"></circle>
                                <circle cx="20" cy="21" r="1"></circle>
                                <path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path>
                            </svg>
                            Games
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if ($_SERVER['REQUEST_URI'] == '/adminPanel/customers') {
                            echo 'active';
                        } ?>" href="https://steamkey.tezgroup.org/adminPanel/customers">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-users">
                                <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                <circle cx="9" cy="7" r="4"></circle>
                                <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                                <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                            </svg>
                            Customers
                        </a>
                    </li>
                </ul>
            </div>
        </nav>

        <main id="main" role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">

            <?php
            if ($_SERVER['REQUEST_URI'] == '/adminPanel/dashboard') {
                require 'Blocks' . DIRECTORY_SEPARATOR . 'dashboard.php';
            } elseif ($_SERVER['REQUEST_URI'] == '/adminPanel/orders') {
                require 'Blocks' . DIRECTORY_SEPARATOR . 'orders.php';
            } elseif ($_SERVER['REQUEST_URI'] == '/adminPanel/games') {
                require 'Blocks' . DIRECTORY_SEPARATOR . 'games.php';
            } elseif ($_SERVER['REQUEST_URI'] == '/adminPanel/customers') {
                require 'Blocks' . DIRECTORY_SEPARATOR . 'customers.php';
            }


            ?>
        </main>
    </div>
</div>
<?php
$today = 0;
$yesterday = 0;
$beforeYesterday = 0;
$postureBeforeYesterday = 0;
$posturePostureBeforeYesterday = 0;
$posturePosturePostureBeforeYesterday = 0;
$posturePosturePosturePostureBeforeYesterday = 0;
foreach ($orders as $order) {
    if (date("Y-m-d") == date("Y-m-d", strtotime($order['created_at']))) {
        $today = $today + 1;
    } elseif (date("Y-m-d", time() - (1 * 24 * 60 * 60)) == date("Y-m-d", strtotime($order['created_at']))) {
        $yesterday = $yesterday + 1;
    } elseif (date("Y-m-d", time() - (2 * 24 * 60 * 60)) == date("Y-m-d", strtotime($order['created_at']))) {
        $beforeYesterday = $beforeYesterday + 1;
    } elseif (date("Y-m-d", time() - (3 * 24 * 60 * 60)) == date("Y-m-d", strtotime($order['created_at']))) {
        $postureBeforeYesterday = $postureBeforeYesterday + 1;
    } elseif (date("Y-m-d", time() - (4 * 24 * 60 * 60)) == date("Y-m-d", strtotime($order['created_at']))) {
        $posturePostureBeforeYesterday = $posturePostureBeforeYesterday + 1;
    } elseif (date("Y-m-d", time() - (5 * 24 * 60 * 60)) == date("Y-m-d", strtotime($order['created_at']))) {
        $posturePosturePostureBeforeYesterday = $posturePosturePostureBeforeYesterday + 1;
    } elseif (date("Y-m-d", time() - (6 * 24 * 60 * 60)) == date("Y-m-d", strtotime($order['created_at']))) {
        $posturePosturePosturePostureBeforeYesterday = $posturePosturePosturePostureBeforeYesterday + 1;
    }
}
?>
<div class="modal fade" id="sendMassage" tabindex="-1" role="dialog" aria-labelledby="sendMassageLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content contact">
            <div class="modal-header">
                <h5 class="modal-title" id="sendMassageLabel">Send message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/massageRestApi" method="post" role="form" class="php-email-form">
                    <div class="form-row">
                        <div class="col-md-12 form-group">
                            <input type="text" name="name" class="form-control" id="nameInput" placeholder="Имя"
                                   data-rule="minlen:2" data-msg="Please enter at least 4 chars">
                            <div class="validate"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 form-group">
                            <input type="text" name="email" class="form-control" id="emailInput" placeholder="Мейл"
                                   data-rule="minlen:4" data-msg="Please enter at least 4 chars">
                            <div class="validate"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 form-group">
                            <input type="text" name="sub" class="form-control" id="sub" placeholder="Заголовок сообщения"
                                   data-rule="minlen:4" data-msg="Please enter at least 4 chars">
                            <div class="validate"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="text">Введите текст сообщения</label>
                        <textarea id="text" class="form-control" name="text" rows="5" data-rule="minlen:3"
                                  data-msg="Please write something for us" placeholder="Message"></textarea>
                        <div class="validate"></div>
                    </div>
                    <div class="mb-3">
                        <div class="loading">Loading</div>
                        <div class="error-message"></div>
                        <div class="sent-message">Your message has been sent. Thank you!</div>
                    </div>

                    <div class="text-center">
                        <button type="submit">Send Message</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script src="../../public/vendor/jquery/jquery.min.js"
        crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>
<script src="../../public/vendor/bootstrap/js/bootstrap.bundle.min.js"
        integrity="sha384-1CmrxMRARb6aLqgBO7yyAxTOQE2AKb9GfXnEo760AUcUmFx3ibVJJAzGytlQcNXd"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
<!-- Vendor JS Files -->
<script src="../public/vendor/php-email-form/validate.js"></script>
<script src="../../public/js/main.js"></script>
<script>
    function sendMassage(name, email) {
        document.getElementById('nameInput').value = name;
        document.getElementById('emailInput').value = email;
    }
    /* globals Chart:false, feather:false */
    (function () {
        'use strict'

        feather.replace()

        // Graphs
        let ctx = document.getElementById('myChart');
        // eslint-disable-next-line no-unused-vars
        let myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    '<?php echo date("l", time() - (6 * 24 * 60 * 60)); ?>',
                    '<?php echo date("l", time() - (5 * 24 * 60 * 60)); ?>',
                    '<?php echo date("l", time() - (4 * 24 * 60 * 60)); ?>',
                    '<?php echo date("l", time() - (3 * 24 * 60 * 60)); ?>',
                    '<?php echo date("l", time() - (2 * 24 * 60 * 60)); ?>',
                    '<?php echo date("l", time() - (1 * 24 * 60 * 60)); ?>',
                    '<?php echo date("l"); ?>'
                ],
                datasets: [{
                    data: [
                        <?php echo $posturePosturePosturePostureBeforeYesterday; ?>,
                        <?php echo $posturePosturePostureBeforeYesterday; ?>,
                        <?php echo $posturePostureBeforeYesterday; ?>,
                        <?php echo $postureBeforeYesterday; ?>,
                        <?php echo $beforeYesterday; ?>,
                        <?php echo $yesterday; ?>,
                        <?php echo $today; ?>,
                    ],
                    lineTension: 0,
                    backgroundColor: 'transparent',
                    borderColor: '#007bff',
                    borderWidth: 4,
                    pointBackgroundColor: '#007bff'
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: false
                        }
                    }]
                },
                legend: {
                    display: false
                }
            }
        })
    }())
</script>
</body>
</html>