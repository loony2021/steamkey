<?php use function Controllers\backgroundBlack;?>
<!-- ======= Header Section ======= -->
<?php require 'Blocks' . DIRECTORY_SEPARATOR . 'head.php'; ?>
<!-- ======= Slider Section ======= -->
<?php require 'Blocks' . DIRECTORY_SEPARATOR . 'slider.php'; ?>

<main id="main">
    <!-- ======= Features Section ======= -->
    <section <?php backgroundBlack(); ?> id="features" class="features services">
        <div class="container">

            <div class="section-title">
                <h2>Features</h2>
                <p>Check our Features</p>
            </div>

            <div class="row">
                <div class="col-lg-9 mt-4 mt-lg-0">
                    <div class="tab-content">
                        <div class="tab-pane active show" id="tab-1">
                            <div class="row">
                                <div <?php backgroundBlack(); ?> class="col-12">
                                    <div <?php backgroundBlack(); ?> class="icon-box">
                                        <img class="float-left mr-3 " src="../../public/img/war2.jpg" alt="">
                                        <h4 class="mt-3"><a href="#">Total War: Warhammer II: The Warden & the
                                                Paunch</a></h4>
                                        <p class="text-secondary">Экшн, Стратегия</p>
                                    </div>
                                </div>
                                <div <?php backgroundBlack(); ?> class="col-12 mt-4 mt-md-0">
                                    <div <?php backgroundBlack(); ?> class="icon-box">
                                        <img class="float-left mr-3 " src="../../public/img/war2.jpg" alt="">
                                        <h4 class="mt-3"><a href="#">Total War: Warhammer II: The Warden & the
                                                Paunch</a></h4>
                                        <p class="text-secondary">Экшн, Стратегия</p>
                                    </div>
                                </div>
                                <div <?php backgroundBlack(); ?> class="col-12 mt-4 mt-md-0">
                                    <div <?php backgroundBlack(); ?> class="icon-box">
                                        <img class="float-left mr-3 " src="../../public/img/war2.jpg" alt="">
                                        <h4 class="mt-3"><a href="#">Total War: Warhammer II: The Warden & the
                                                Paunch</a></h4>
                                        <p class="text-secondary">Экшн, Стратегия</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-2">
                            <div class="row">
                                <div <?php backgroundBlack(); ?> class="col-12">
                                    <div <?php backgroundBlack(); ?> class="icon-box">
                                        <img class="float-left mr-3 " src="../../public/img/war2.jpg" alt="">
                                        <h4 class="mt-3"><a href="#">Total War: Warhammer II: The Warden & the
                                                Paunch</a></h4>
                                        <p class="text-secondary">Экшн, Стратегия</p>
                                    </div>
                                </div>
                                <div <?php backgroundBlack(); ?> class="col-12 mt-4 mt-md-0">
                                    <div <?php backgroundBlack(); ?> class="icon-box">
                                        <i class="icofont-chart-bar-graph"></i>
                                        <h4><a href="#">Dolor Sitema</a></h4>
                                        <p>Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                            commodo consequat tarad limino ata</p>
                                    </div>
                                </div>
                                <div <?php backgroundBlack(); ?> class="col-12 mt-4 mt-md-0">
                                    <div <?php backgroundBlack(); ?> class="icon-box">
                                        <i class="icofont-image"></i>
                                        <h4><a href="#">Sed ut perspiciatis</a></h4>
                                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
                                            eu fugiat nulla pariatur</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-3">
                            <div class="row">
                                <div <?php backgroundBlack(); ?> class="col-12">
                                    <div <?php backgroundBlack(); ?> class="icon-box">
                                        <img class="float-left mr-3 " src="../../public/img/war2.jpg" alt="">
                                        <h4 class="mt-3"><a href="#">Total War: Warhammer II: The Warden & the
                                                Paunch</a></h4>
                                        <p class="text-secondary">Экшн, Стратегия</p>
                                    </div>
                                </div>
                                <div <?php backgroundBlack(); ?> class="col-12 mt-4 mt-md-0">
                                    <div <?php backgroundBlack(); ?> class="icon-box">
                                        <i class="icofont-chart-bar-graph"></i>
                                        <h4><a href="#">Dolor Sitema</a></h4>
                                        <p>Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                            commodo consequat tarad limino ata</p>
                                    </div>
                                </div>
                                <div <?php backgroundBlack(); ?> class="col-12 mt-4 mt-md-0">
                                    <div <?php backgroundBlack(); ?> class="icon-box">
                                        <i class="icofont-image"></i>
                                        <h4><a href="#">Sed ut perspiciatis</a></h4>
                                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
                                            eu fugiat nulla pariatur</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div <?php backgroundBlack(); ?> class="col-lg-3">
                    <ul <?php backgroundBlack(); ?> class="nav nav-tabs flex-column">
                        <li <?php backgroundBlack(); ?> class="nav-item">
                            <a class="nav-link active show" data-toggle="tab" href="#tab-1">Лидеры продаж</a>
                        </li>
                        <li <?php backgroundBlack(); ?> class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab-2">Новинки</a>
                        </li>
                        <li <?php backgroundBlack(); ?> class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab-3">Ожтдаемые</a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </section><!-- End Features Section -->

    <!-- ======= Portfolio Section ======= -->
    <section <?php backgroundBlack(); ?> id="portfolio" class="portfolio">
        <div class="container">

            <div class="section-title">
                <h2>Игры</h2>
                <p>По жанрам</p>
            </div>

            <div class="row">
                <div class="col-lg-12 d-flex justify-content-center">
                    <ul id="portfolio-flters">
                        <li data-filter="*" class="filter-active">Все</li>
                        <li data-filter=".filter-app">Экшн</li>
                        <li data-filter=".filter-card">Стратегии</li>
                        <li data-filter=".filter-web">Платформеры</li>
                    </ul>
                </div>
            </div>

            <div class="row portfolio-container">

                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <div class="portfolio-wrap">
                        <img src="../../public/img/fnw.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>App 1</h4>
                            <p>App</p>
                            <div class="portfolio-links">
                                <a href="#" data-gall="portfolioGallery"
                                   class="venobox" title="App 1"><i class="bx bx-plus"></i></a>
                                <a href="#" data-gall="portfolioDetailsGallery"
                                   data-vbtype="iframe" class="venobox" title="Portfolio Details"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <div class="portfolio-wrap">
                        <img src="../../public/img/valhalla.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>App 1</h4>
                            <p>App</p>
                            <div class="portfolio-links">
                                <a href="#" data-gall="portfolioGallery"
                                   class="venobox" title="App 1"><i class="bx bx-plus"></i></a>
                                <a href="#" data-gall="portfolioDetailsGallery"
                                   data-vbtype="iframe" class="venobox" title="Portfolio Details"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <div class="portfolio-wrap">
                        <img src="../../public/img/civilization.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>App 1</h4>
                            <p>App</p>
                            <div class="portfolio-links">
                                <a href="#" data-gall="portfolioGallery"
                                   class="venobox" title="App 1"><i class="bx bx-plus"></i></a>
                                <a href="#" data-gall="portfolioDetailsGallery"
                                   data-vbtype="iframe" class="venobox" title="Portfolio Details"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <div class="portfolio-wrap">
                        <img src="../../public/img/cp2077.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>App 1</h4>
                            <p>App</p>
                            <div class="portfolio-links">
                                <a href="#" data-gall="portfolioGallery"
                                   class="venobox" title="App 1"><i class="bx bx-plus"></i></a>
                                <a href="#" data-gall="portfolioDetailsGallery"
                                   data-vbtype="iframe" class="venobox" title="Portfolio Details"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                    <div class="portfolio-wrap">
                        <img src="../../public/img/csgo.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Web 3</h4>
                            <p>Web</p>
                            <div class="portfolio-links">
                                <a href="#" data-gall="portfolioGallery"
                                   class="venobox" title="Web 3"><i class="bx bx-plus"></i></a>
                                <a href="#" data-gall="portfolioDetailsGallery"
                                   data-vbtype="iframe" class="venobox" title="Portfolio Details"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <div class="portfolio-wrap">
                        <img src="../../public/img/war2.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>App 2</h4>
                            <p>App</p>
                            <div class="portfolio-links">
                                <a href="#" data-gall="portfolioGallery"
                                   class="venobox" title="App 2"><i class="bx bx-plus"></i></a>
                                <a href="#" data-gall="portfolioDetailsGallery"
                                   data-vbtype="iframe" class="venobox" title="Portfolio Details"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                    <div class="portfolio-wrap">
                        <img src="../../public/img/rdr2.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Card 2</h4>
                            <p>Card</p>
                            <div class="portfolio-links">
                                <a href="#" data-gall="portfolioGallery"
                                   class="venobox" title="Card 2"><i class="bx bx-plus"></i></a>
                                <a href="#" data-gall="portfolioDetailsGallery"
                                   data-vbtype="iframe" class="venobox" title="Portfolio Details"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                    <div class="portfolio-wrap">
                        <img src="../../public/img/mab2.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Web 2</h4>
                            <p>Web</p>
                            <div class="portfolio-links">
                                <a href="#" data-gall="portfolioGallery"
                                   class="venobox" title="Web 2"><i class="bx bx-plus"></i></a>
                                <a href="#" data-gall="portfolioDetailsGallery"
                                   data-vbtype="iframe" class="venobox" title="Portfolio Details"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <div class="portfolio-wrap">
                        <img src="../../public/img/kenshi.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>App 3</h4>
                            <p>App</p>
                            <div class="portfolio-links">
                                <a href="#" data-gall="portfolioGallery"
                                   class="venobox" title="App 3"><i class="bx bx-plus"></i></a>
                                <a href="#" data-gall="portfolioDetailsGallery"
                                   data-vbtype="iframe" class="venobox" title="Portfolio Details"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                    <div class="portfolio-wrap">
                        <img src="../../public/img/warhammer.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Card 1</h4>
                            <p>Card</p>
                            <div class="portfolio-links">
                                <a href="#" data-gall="portfolioGallery"
                                   class="venobox" title="Card 1"><i class="bx bx-plus"></i></a>
                                <a href="#" data-gall="portfolioDetailsGallery"
                                   data-vbtype="iframe" class="venobox" title="Portfolio Details"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                    <div class="portfolio-wrap">
                        <img src="../../public/img/gta5.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Card 3</h4>
                            <p>Card</p>
                            <div class="portfolio-links">
                                <a href="#" data-gall="portfolioGallery"
                                   class="venobox" title="Card 3"><i class="bx bx-plus"></i></a>
                                <a href="#" data-gall="portfolioDetailsGallery"
                                   data-vbtype="iframe" class="venobox" title="Portfolio Details"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                    <div class="portfolio-wrap">
                        <img src="../../public/img/witcher3.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Web 3</h4>
                            <p>Web</p>
                            <div class="portfolio-links">
                                <a href="#" data-gall="portfolioGallery"
                                   class="venobox" title="Web 3"><i class="bx bx-plus"></i></a>
                                <a href="#" data-gall="portfolioDetailsGallery"
                                   data-vbtype="iframe" class="venobox" title="Portfolio Details"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section><!-- End Portfolio Section -->

    <!-- ======= About Section ======= -->
    <section <?php backgroundBlack(); ?> id="about" class="about">
        <div class="container">

            <div class="row content">
                <div class="col-lg-6">
                    <h2>Что такое СтимКей?</h2>
                    <h3>В нашем интернет-магазине вы сможете гарантированно приобрести ключи к играм от Steam и прочих
                        популярных игровых платформ. Наш магазин делает все для того, чтобы ваши
                        покупки проходили быстро, с максимальным удобством и безопасностью, а цены оставались
                        максимально доступными.</h3>
                </div>
                <div class="col-lg-6 pt-4 pt-lg-0">
                    <h3>Наши преимущества</h3>
                    <ul>
                        <li><i class="ri-check-double-line"></i> широкий ассортимент игр – более 2 000 позиций в
                            каталоге
                        </li>
                        <li><i class="ri-check-double-line"></i> техническая поддержка сайта поможет ответить на
                            возникшие вопросы и решить их.
                        </li>
                        <li><i class="ri-check-double-line"></i> наша аттестация в электронных платежных системах
                            позволяет работать с самыми распространенными электронными кошельками: Вебмани, Киви, Яндекс
                        </li>
                        <li><i class="ri-check-double-line"></i>принимаем и оплату по банковской карте (Visa,
                            MasterCard)
                        </li>
                        <li><i class="ri-check-double-line"></i>Мобильные операторы (Билайн, ТЕЛЕ2, Мегафон и МТС)
                        </li>
                        <li><i class="ri-check-double-line"></i>Интернет-банкинг (Сбербанк.Онлайн, Альфа, Русский
                            стандарт, ВТБ24, Промсвязьбанк)
                        </li>
                        <li><i class="ri-check-double-line"></i>накопительная система скидок до 10%. Чем больше сумма
                            покупок - тем больше скидка на последующие покупки.
                        </li>
                        <li><i class="ri-check-double-line"></i>Мы приобретаем ключи оптом у официальных дилеров, работающих напрямую с издателями
                        </li>
                        <li><i class="ri-check-double-line"></i>Регулярно мониторим другие крупные онлайн-магазины и готовы предложить самую низкую цену.
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </section><!-- End About Section -->

    <!-- ======= Contact Section ======= -->
    <?php require 'Blocks' . DIRECTORY_SEPARATOR . 'order.php'; ?><!-- End Contact Section -->


</main><!-- End #main -->

<?php require 'Blocks' . DIRECTORY_SEPARATOR . 'footer.php'; ?>