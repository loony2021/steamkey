<?php


namespace Config;

    class Routs {
        public const APP_ROUTS = [
            '/' => '\Controllers\Home@home',
            '/adminPanelLogin' => '\Controllers\Admin@login',
            '/adminPanel/dashboard' => '\Controllers\Admin@adminPanel',
            '/adminPanel/orders' => '\Controllers\Admin@adminPanel',
            '/adminPanel/games' => '\Controllers\Admin@adminPanel',
            '/adminPanel/customers' => '\Controllers\Admin@adminPanel',

            '/adminRestApiLogin' => '\RestApi\Admin@login',
            '/adminRestApiLogout' => '\RestApi\Admin@logout',
            '/orderRestApi' => '\RestApi\Order@create',
            '/addGameRestApi' => '\RestApi\Game@create',
            '/massageRestApi' => '\RestApi\Mail@sendMassage',
        ];
    }

